#!/usr/bin/env python3
import os
import rospy
import cv2
from cv_bridge import CvBridge
from PIL import Image as Img
import numpy as np
from math import pi

from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3

from sensor_msgs.msg import Image
from sensor_msgs.msg import Range

class Control(object):

    def __init__(self):
        rospy.loginfo("[Control] loaging")
        rospy.on_shutdown(self.shutdown)

        self.gui = os.getenv('GUI')=='true' or os.getenv('GUI')=='True'

        self.cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=3)
        self.odom_subscriber = rospy.Subscriber("/odom", Odometry, self.odom_callback)
        self.camera_subscriber = rospy.Subscriber("/camera/publish_coor", Int32MultiArray, self.camera_cb)
        self.range_front_subscriber = rospy.Subscriber("/sonar_publish_range", Float32, self.range_front_callback)
        
        self.odometry = Odometry()
        self.x_pos = 0
        self.y_pos = 0
        self.z_ang = 0

        self.command = Twist()

        self.sonar_data = 0
        self.sonar_limit_forward = 0.4

        self.curent_image = None
        self.bridge = CvBridge()

        self.x1_tax,    self.y1_tax,    self.x2_tax,    self.y2_tax    = [0, 0, 0, 0]
        self.x1_reward, self.y1_reward, self.x2_reward, self.y2_reward = [0, 0, 0, 0]

        self.x_target, self.y_target = [0, 0]

        # PID
        self.p_x        = 0.8
        self.p_z        = 1.2
        self.p_wall     = 0.002
        self.p_z_target = 0.002

        self.angle_odom = 0

    def __del__(self):
        pass

    def shutdown(self):
        # stop robots here
        self.cmd_vel.publish(Twist())

    def camera_cb(self, msg):
        self.x1_tax,    self.y1_tax,    self.x2_tax,    self.y2_tax    = msg.data[0], msg.data[1], msg.data[2], msg.data[3]
        self.x1_reward, self.y1_reward, self.x2_reward, self.y2_reward = msg.data[4], msg.data[5], msg.data[6], msg.data[7]

    def range_front_callback(self, msg):
        self.sonar_data = msg.data

    def odom_callback(self, msg: Odometry):
        self.odometry = msg
        self.x_pos = msg.pose.pose.position.x
        self.y_pos = msg.pose.pose.position.y
        self.z_ang = msg.pose.pose.orientation.z

    def spin(self):
        rate = rospy.Rate(30)
        t0 = rospy.get_time()

        while not rospy.is_shutdown():
            t = rospy.get_time() - t0

            self.command.linear.x  = 0.0
            self.command.linear.y  = 0.0
            self.command.angular.z = 0.0

            self.turn_90()

            # if self.sonar_data < self.sonar_limit_forward or self.angle_odom:
            #     self.turn_90()

            self.cmd_vel.publish(self.command)
            rospy.sleep(1)

    def turn_90(self):
        k = ((2*self.z_ang/pi) * 100) % 100 + 2

        if k != 0:
            if not self.angle_odom == 0:
                self.command.linear.x  = -0.2
                self.command.angular.z = 1.68
                self.angle_odom = 1
            else:
                self.command.angular.z = (100 - k) * 0.015
        else: self.angle_odom = 0


def main(args=None):
    rospy.init_node("control_node")

    exp = Control()
    exp.spin()


if __name__ == "__main__":
    main()